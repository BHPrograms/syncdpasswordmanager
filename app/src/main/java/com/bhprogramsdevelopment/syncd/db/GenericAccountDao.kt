package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.CreditCard
import com.bhprogramsdevelopment.syncd.models.EmailAccount
import com.bhprogramsdevelopment.syncd.models.GenericAccount
import io.reactivex.Flowable

//Data access object for GenericAccount table
@Dao
interface GenericAccountDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createGenericAccount(genericAccount: GenericAccount): Long

    @Query("SELECT * FROM GenericAccount")
    fun getAllGenericAccounts(): Flowable<List<GenericAccount>>

    @Query("SELECT * FROM GenericAccount WHERE account_name LIKE :accountName")
    fun getGenericAccount(accountName: String): GenericAccount

    @Delete
    fun deleteGenericAccount(genericAccount: GenericAccount)
}