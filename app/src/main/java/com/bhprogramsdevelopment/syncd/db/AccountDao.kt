package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.Account
import com.bhprogramsdevelopment.syncd.models.CreditCard
import io.reactivex.Flowable

//Data access object for Account table
@Dao
interface AccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createAccount(account: Account): Long

    @Query("SELECT * FROM Account")
    fun getAllAccounts(): Flowable<List<Account>>

    @Query("SELECT * FROM Account")
    fun getCurrentAccounts(): List<Account>

    @Delete
    fun deleteAccount(account: Account)

    @Query("SELECT * FROM CreditCard WHERE account_name LIKE :accountName")
    fun getAccount(accountName: String): Account

    @Query("SELECT EXISTS(SELECT * FROM Account WHERE account_name = :accountName)")
    fun accountExists(accountName : String) : Boolean
}