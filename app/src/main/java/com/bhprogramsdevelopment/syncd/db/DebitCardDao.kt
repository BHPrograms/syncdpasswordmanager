package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.CreditCard
import com.bhprogramsdevelopment.syncd.models.DebitCard
import com.bhprogramsdevelopment.syncd.models.EmailAccount
import io.reactivex.Flowable

//Data access object for DebitCard table
@Dao
interface DebitCardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createDebitCard(debitCard: DebitCard): Long

    @Query("SELECT * FROM DebitCard")
    fun getAllDebitCards(): Flowable<List<DebitCard>>

    @Query("SELECT * FROM DebitCard WHERE account_name LIKE :accountName")
    fun getDebitCard(accountName: String): DebitCard

    @Delete
    fun deleteDebitCard(debitCard: DebitCard)
}