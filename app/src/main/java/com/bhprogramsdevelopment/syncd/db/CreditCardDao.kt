package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.CreditCard
import io.reactivex.Flowable

//Data access object for CreditCard table
@Dao
interface CreditCardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createCreditCard(creditCard: CreditCard): Long

    @Query("SELECT * FROM CreditCard")
    fun getAllCreditCards(): Flowable<List<CreditCard>>

    @Query("SELECT * FROM CreditCard WHERE account_name LIKE :accountName")
    fun getCreditCard(accountName: String): CreditCard

    @Delete
    fun deleteCreditCard(creditCard: CreditCard)
}