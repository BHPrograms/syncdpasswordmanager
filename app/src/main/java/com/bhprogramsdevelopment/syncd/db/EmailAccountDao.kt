package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.CreditCard
import com.bhprogramsdevelopment.syncd.models.EmailAccount
import io.reactivex.Flowable

//Data access object for EmailAccount table
@Dao
interface EmailAccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createEmailAccount(emailAccount: EmailAccount): Long

    @Query("SELECT * FROM EmailAccount")
    fun getAllEmailAccounts(): Flowable<List<EmailAccount>>

    @Query("SELECT * FROM EmailAccount WHERE account_name LIKE :accountName")
    fun getEmailAccount(accountName: String): EmailAccount

    @Delete
    fun deleteEmailAccount(emailAccount: EmailAccount)
}