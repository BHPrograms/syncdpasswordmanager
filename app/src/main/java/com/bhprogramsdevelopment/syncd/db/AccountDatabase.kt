package com.bhprogramsdevelopment.syncd.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.Room.*
import androidx.room.RoomDatabase
import com.bhprogramsdevelopment.syncd.models.*
import com.bhprogramsdevelopment.syncd.utilities.DATABASE_NAME

//Local room database metadata and structure
@Database(entities = arrayOf(Account::class, CreditCard::class, DebitCard::class,
    EmailAccount::class, GenericAccount::class, OnlineBankAccount::class), version = 1, exportSchema = false)
abstract class AccountDatabase: RoomDatabase() {

    //Implements singleton pattern
    companion object {
        private var instance: AccountDatabase? = null

        fun getInstance(context: Context): AccountDatabase? {
            if (instance == null) {
                synchronized(AccountDatabase::class.java) {
                    instance =  Room.databaseBuilder(context.applicationContext,
                        AccountDatabase::class.java, DATABASE_NAME).build()
                }
            }
            return instance;
        }
    }

    abstract fun AccountDao(): AccountDao
    abstract fun CreditCardDao(): CreditCardDao
    abstract fun DebitCardDao(): DebitCardDao
    abstract fun EmailAccountDao(): EmailAccountDao
    abstract fun GenericAccountDao(): GenericAccountDao
    abstract fun OnlineBankAccountDao(): OnlineBankAccountDao
}