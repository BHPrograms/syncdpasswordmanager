package com.bhprogramsdevelopment.syncd.db

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.data.AppSync
import com.bhprogramsdevelopment.syncd.models.*
import com.bhprogramsdevelopment.syncd.utilities.KEY_SYNC_ENABLED
import com.bhprogramsdevelopment.syncd.utilities.SharedPrefs
import com.google.gson.Gson
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//Holds all references to DB and DAO
class AccountDataSource(context: Context) {
    //region DAO member variables

    //Dao for all entities
    private var AccountDao: AccountDao? = null
    private var CreditCardDao: CreditCardDao? = null
    private var DebitCardDao: DebitCardDao? = null
    private var EmailAccountDao: EmailAccountDao? = null
    private var GenericAccountDao: GenericAccountDao? = null
    private var OnlineBankAccountDao: OnlineBankAccountDao? = null

    //endregion

    //region Data member variables

    //Used to interact with remote data source, check current settings
    private var appSync: AppSync
    private var sharedPrefs: SharedPrefs

    //endregion

    //region Initialization

    //AccountDataSource acts as singleton, providing data for all activities/fragments
    companion object {
        private var instance: AccountDataSource? = null

        fun getInstance(context: Context): AccountDataSource {
            if (instance == null) {
                instance = AccountDataSource(context)
                return instance as AccountDataSource
            }

            return instance as AccountDataSource
        }
    }

    //Initializes Daos for static instance
    init {
        val db = AccountDatabase.getInstance(context)
        AccountDao = db?.AccountDao()
        CreditCardDao = db?.CreditCardDao()
        DebitCardDao = db?.DebitCardDao()
        EmailAccountDao = db?.EmailAccountDao()
        GenericAccountDao = db?.GenericAccountDao()
        OnlineBankAccountDao = db?.OnlineBankAccountDao()
        appSync = AppSync.getInstance(context)
        sharedPrefs = SharedPrefs.getInstance(context)
    }

    //endregion

    //region Local & Remote account manipulation

    //Creates an account listing locally, and a full account remotely if sync enabled
    fun createAccount(accountName: String, accountType: String, json: String) {
        val account = Account(accountName, accountType, "")
        AccountDao?.createAccount(account)

        if (sharedPrefs.getBool(KEY_SYNC_ENABLED)) {
            appSync.createUserAccount(json)
        }
    }

    //Deletes local account, and if enabled, corresponding remote account
    fun deleteAccount(gsonString: String) {
        val accountListing = Gson().fromJson(gsonString, Account::class.java)
        val accountType = accountListing.account_type

        //Based on type from accountListing, populate UI
        when (accountType) {
            "CreditCard" -> {
                val account = Gson().fromJson(gsonString, CreditCard::class.java)
                CreditCardDao?.deleteCreditCard(account)
            }

            "DebitCard" -> {
                val account = Gson().fromJson(gsonString, DebitCard::class.java)
                DebitCardDao?.deleteDebitCard(account)
            }

            "EmailAccount" -> {
                val account = Gson().fromJson(gsonString, EmailAccount::class.java)
                EmailAccountDao?.deleteEmailAccount(account)
            }

            "GenericAccount" -> {
                val account = Gson().fromJson(gsonString, GenericAccount::class.java)
                GenericAccountDao?.deleteGenericAccount(account)
            }

            "OnlineBankAccount" -> {
                val account = Gson().fromJson(gsonString, OnlineBankAccount::class.java)
                OnlineBankAccountDao?.deleteOnlineBankAccount(account)
            }
        }

        AccountDao?.deleteAccount(accountListing)

        if (accountListing.appsync_id.isNotBlank() && sharedPrefs.getBool(KEY_SYNC_ENABLED)) {
            appSync.deleteUserAccount(accountListing)
        }

    }

    //Updates account locally, and if enabled, syncs account remotely or creates if needed
    fun updateAccount(gsonString: String) {
        val accountListing = Gson().fromJson(gsonString, Account::class.java)
        val accountType = accountListing.account_type

        //Based on type from accountListing, populate UI
        when (accountType) {
            "CreditCard" -> {
                val account = Gson().fromJson(gsonString, CreditCard::class.java)
                CreditCardDao?.createCreditCard(account)
            }

            "DebitCard" -> {
                val account = Gson().fromJson(gsonString, DebitCard::class.java)
                DebitCardDao?.createDebitCard(account)
            }

            "EmailAccount" -> {
                val account = Gson().fromJson(gsonString, EmailAccount::class.java)
                EmailAccountDao?.createEmailAccount(account)
            }

            "GenericAccount" -> {
                val account = Gson().fromJson(gsonString, GenericAccount::class.java)
                GenericAccountDao?.createGenericAccount(account)
            }

            "OnlineBankAccount" -> {
                val account = Gson().fromJson(gsonString, OnlineBankAccount::class.java)
                OnlineBankAccountDao?.createOnlineBankAccount(account)
            }
        }

        AccountDao?.createAccount(accountListing)

        if (sharedPrefs.getBool(KEY_SYNC_ENABLED)) {
            if (accountListing.appsync_id.isBlank()) {
                appSync.createUserAccount(gsonString)
            } else {
                appSync.updateUserAccount(accountListing, gsonString)
            }
        }
    }

    //Updates local account from remote data, does not trigger sync with remote
    fun updateAccountWithRemote(gsonString: String, id: String) {
        val accountListing = Gson().fromJson(gsonString, Account::class.java)
        accountListing.appsync_id = id
        //Based on type from accountListing, populate UI
        when (accountListing.account_type) {
            "CreditCard" -> {
                val account = Gson().fromJson(gsonString, CreditCard::class.java)
                account.appsync_id = id
                CreditCardDao?.createCreditCard(account)
            }

            "DebitCard" -> {
                val account = Gson().fromJson(gsonString, DebitCard::class.java)
                account.appsync_id = id
                DebitCardDao?.createDebitCard(account)
            }

            "EmailAccount" -> {
                val account = Gson().fromJson(gsonString, EmailAccount::class.java)
                account.appsync_id = id
                EmailAccountDao?.createEmailAccount(account)
            }

            "GenericAccount" -> {
                val account = Gson().fromJson(gsonString, GenericAccount::class.java)
                account.appsync_id = id
                GenericAccountDao?.createGenericAccount(account)
            }

            "OnlineBankAccount" -> {
                val account = Gson().fromJson(gsonString, OnlineBankAccount::class.java)
                account.appsync_id = id
                OnlineBankAccountDao?.createOnlineBankAccount(account)
            }
        }

        AccountDao?.createAccount(accountListing)
    }

    //Retrieves all account listings in an observable format
    fun getAllAccounts(): Flowable<List<Account>>? {
        return AccountDao?.getAllAccounts()
    }

    //Checks for accounts with the same name, returns true if any are found
    fun checkAccountExists(accountName: String): Boolean {
        return AccountDao?.accountExists(accountName) ?: false
    }

    //endregion

    ///region Remote functions

    //Syncs all local accounts with remote accounts
    fun syncWithRemote() {
        CoroutineScope(Dispatchers.IO).launch {
            val accounts = AccountDao?.getCurrentAccounts()

            if (accounts != null) {
                for (account in accounts) {
                    if (account.appsync_id.isBlank()) {
                        var json = ""
                        when (account.account_type) {
                            "CreditCard" -> {
                                json =
                                    Gson().toJson(CreditCardDao?.getCreditCard(account.account_name))
                            }

                            "DebitCard" -> {
                                json =
                                    Gson().toJson(DebitCardDao?.getDebitCard(account.account_name))
                            }

                            "EmailAccount" -> {
                                json =
                                    Gson().toJson(EmailAccountDao?.getEmailAccount(account.account_name))
                            }

                            "GenericAccount" -> {
                                json =
                                    Gson().toJson(GenericAccountDao?.getGenericAccount(account.account_name))
                            }

                            "OnlineBankAccount" -> {
                                json =
                                    Gson().toJson(OnlineBankAccountDao?.getOnlineBankAccount(account.account_name))
                            }
                        }
                        appSync.createUserAccount(json)
                    }
                }
            }
        }
    }

    //Deletes all local accounts that no longer exist remotely
    fun deleteOrphans(validIDs: ArrayList<String>) {
        CoroutineScope(Dispatchers.IO).launch {
            val accounts = AccountDao?.getCurrentAccounts()

            if (accounts != null) {
                for (account in accounts) {
                    if (account.appsync_id.isNotBlank() && !validIDs.contains(account.appsync_id)) {
                        var json = ""
                        when (account.account_type) {
                            "CreditCard" -> {
                                json =
                                    Gson().toJson(CreditCardDao?.getCreditCard(account.account_name))
                            }

                            "DebitCard" -> {
                                json =
                                    Gson().toJson(DebitCardDao?.getDebitCard(account.account_name))
                            }

                            "EmailAccount" -> {
                                json =
                                    Gson().toJson(EmailAccountDao?.getEmailAccount(account.account_name))
                            }

                            "GenericAccount" -> {
                                json =
                                    Gson().toJson(GenericAccountDao?.getGenericAccount(account.account_name))
                            }

                            "OnlineBankAccount" -> {
                                json =
                                    Gson().toJson(OnlineBankAccountDao?.getOnlineBankAccount(account.account_name))
                            }
                        }
                        deleteAccount(json)
                    }
                }
            }
        }
    }

    //endregion

    //region Functions for account models
    fun createCreditCardAccount(accountName: String, notes: String, cardProvider: String,
        cardHolder: String, cardNumber: String, cvc: String, expirationDate: String) {
        val account = CreditCard(
            accountName,
            notes = notes,
            card_provider = cardProvider,
            card_holder = cardHolder,
            card_number = cardNumber,
            cvc = cvc,
            expiration_date = expirationDate,
            appsync_id = ""
        )
        CreditCardDao?.createCreditCard(account)
        createAccount(accountName, "CreditCard", Gson().toJson(account))
    }

    fun getCreditCard(accountName: String): CreditCard? {
        return CreditCardDao?.getCreditCard(accountName)
    }

    fun createDebitCardAccount(accountName: String, notes: String, cardProvider: String,
        cardHolder: String, cardNumber: String, cvc: String, expirationDate: String, pin: String) {
        val account = DebitCard(
            accountName,
            notes = notes,
            card_provider = cardProvider,
            card_holder = cardHolder,
            card_number = cardNumber,
            cvc = cvc,
            expiration_date = expirationDate,
            pin = pin,
            appsync_id = ""
        )
        DebitCardDao?.createDebitCard(account)
        createAccount(accountName, "DebitCard", Gson().toJson(account))
    }

    fun getDebitCard(accountName: String): DebitCard? {
        return DebitCardDao?.getDebitCard(accountName)
    }

    fun createEmailAccount(accountName: String, notes: String, emailProvider: String, email: String,
        password: String) {
        val account = EmailAccount(
            account_name = accountName, notes = notes, email_provider = emailProvider,
            email = email, password = password, appsync_id = ""
        )
        EmailAccountDao?.createEmailAccount(account)
        createAccount(accountName, "EmailAccount", Gson().toJson(account))
    }

    fun getEmailAccount(accountName: String): EmailAccount? {
        return EmailAccountDao?.getEmailAccount(accountName)
    }

    fun createGenericAccount(accountName: String, accountProvider: String, username: String, password: String,
        notes: String) {
        val account = GenericAccount(
            account_name = accountName, account_provider = accountProvider,
            username = username, password = password, notes = notes, appsync_id = ""
        )
        GenericAccountDao?.createGenericAccount(account)
        createAccount(accountName, "GenericAccount", Gson().toJson(account))
    }

    fun getGenericAccount(accountName: String): GenericAccount? {
        return GenericAccountDao?.getGenericAccount(accountName)
    }

    fun createOnlineBankAccount(accountName: String, notes: String, bankName: String,
        accountNumber: String, username: String, password: String, pin: String) {
        val account = OnlineBankAccount(
            account_name = accountName,
            notes = notes,
            bank_name = bankName,
            account_number = accountNumber,
            username = username,
            password = password,
            pin = pin,
            appsync_id = ""
        )
        OnlineBankAccountDao?.createOnlineBankAccount(account)
        createAccount(accountName, "OnlineBankAccount", Gson().toJson(account))
    }

    fun getOnlineBankAccount(accountName: String): OnlineBankAccount? {
        return OnlineBankAccountDao?.getOnlineBankAccount(accountName)
    }
    //endregion
}