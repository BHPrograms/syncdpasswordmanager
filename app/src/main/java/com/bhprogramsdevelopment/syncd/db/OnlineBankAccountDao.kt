package com.bhprogramsdevelopment.syncd.db

import androidx.room.*
import com.bhprogramsdevelopment.syncd.models.CreditCard
import com.bhprogramsdevelopment.syncd.models.OnlineBankAccount
import io.reactivex.Flowable

//Data access object for OnlineBankAccount table
@Dao
interface OnlineBankAccountDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createOnlineBankAccount(onlineBankAccount: OnlineBankAccount): Long

    @Query("SELECT * FROM OnlineBankAccount")
    fun getAllOnlineBankAccounts(): Flowable<List<OnlineBankAccount>>

    @Query("SELECT * FROM OnlineBankAccount WHERE account_name LIKE :accountName")
    fun getOnlineBankAccount(accountName: String): OnlineBankAccount

    @Delete
    fun deleteOnlineBankAccount(onlineBankAccount: OnlineBankAccount)
}