package com.bhprogramsdevelopment.syncd.utilities

import android.content.Context
import android.content.SharedPreferences

class SharedPrefs(context: Context) {
    private var sharedPrefs: SharedPreferences =
        context.getSharedPreferences(sharedPrefsKey, Context.MODE_PRIVATE)

    companion object {
        private var instance: SharedPrefs? = null

        fun getInstance(context: Context): SharedPrefs {
            if (instance == null) {
                instance = SharedPrefs(context)
            }
            return instance as SharedPrefs
        }
    }

    //region Preference manipulation
    fun deletePref(key: String) {
        with (sharedPrefs.edit()) {
            remove(key)
            apply()
        }
    }

    fun putString(key: String, value: String) {
        with (sharedPrefs.edit()) {
            putString(key, value)
            apply()
        }
    }

    fun getString(key: String): String {
        return sharedPrefs.getString(key, sharedPrefsDefaultString)!!
    }

    fun putBool(key: String, value: Boolean) {
        with (sharedPrefs.edit()) {
            putBoolean(key, value)
            apply()
        }
    }

    fun getBool(key: String): Boolean {
        return sharedPrefs.getBoolean(key, false)
    }

    //endregion

    //region Utility methods

    //Instead of directly returning a shared pref, respond resource id for saved style
    fun getStyle(): Int {
        val theme = getString(KEY_APP_THEME)

        if (theme == appThemeDark)
            return appThemeDarkStyle
        return appThemeLightStyle
    }

    //endregion

}