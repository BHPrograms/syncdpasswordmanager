package com.bhprogramsdevelopment.syncd.utilities

import com.bhprogramsdevelopment.syncd.R

//Shared preferences constants
const val sharedPrefsKey = "SyncdPreferences"
const val sharedPrefsDefaultString = ""

//region Legal agreement prefs constants
const val privacyPolicyPrefsKey = "PrivacyPolicy"
const val termsAndConditionsPrefsKey = "TermsAndConditions"
//endregion

//region Theme constants
const val appThemeDark = "MaterialDark"
const val appThemeLightStyle = R.style.AppTheme
const val appThemeDarkStyle = R.style.DarkAppTheme
//endregion

//region Prefs Defaults
const val appPinDefault = "null"
const val appThemeDefault = "Light"
//endregion

//region SharedPrefs keys
const val KEY_APP_THEME = "AppTheme"
const val KEY_APP_PIN = "AppPin"
const val KEY_APP_PIN_ENABLED = "AppPinEnabled"
const val KEY_SYNC_ENABLED = "SyncEnabled"
const val KEY_SYNC_PIN_ENABLED = "SyncPinEnabled"
const val KEY_SYNC_PIN = "SyncPin"
//endregion

//region Request Codes
const val reqSettingsCode = 0
//endregion

//region Extra Keys
const val extraThemeChanged = "ThemeChanged"
//endregion

//region Regex
const val regexEmail = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}\$"
//endregion

//region DB
const val DATABASE_NAME = "SyncdDatabase"
//endregion