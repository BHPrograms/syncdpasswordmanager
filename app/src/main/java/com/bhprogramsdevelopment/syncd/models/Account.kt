package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*

/*Parent of all account items, used to store info for RecyclerView */
@Entity(indices = arrayOf(Index(value = ["account_name"], unique = true)), inheritSuperIndices = true)
open class Account(account_name: String, account_type: String, appsync_id: String) {
    @PrimaryKey
    var account_name = account_name
    var account_type = account_type
    var appsync_id = appsync_id
}