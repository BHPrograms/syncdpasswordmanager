package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.util.*

@Entity
class EmailAccount(account_name: String, notes: String, email_provider: String, email: String,
                   password: String,  appsync_id: String): Account(account_name, "EmailAccount",  appsync_id) {
    var email_provider = email_provider
    var email = email
    var password = password
    var notes = notes
}