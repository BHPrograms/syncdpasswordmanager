package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(inheritSuperIndices = true)
class OnlineBankAccount(account_name: String, notes: String, bank_name: String,
account_number: String, username: String, password: String, pin: String,  appsync_id: String):
    GenericAccount(account_name, bank_name, username, password, notes, "OnlineBankAccount",  appsync_id) {
    var bank_name = bank_name
    var account_number = account_number
    var pin = pin
}