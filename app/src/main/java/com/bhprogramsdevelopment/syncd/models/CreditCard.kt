package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(inheritSuperIndices = true)
open class CreditCard(account_name: String, notes: String, card_provider: String, card_holder: String, card_number: String,
    cvc: String, expiration_date: String, account_type: String = "CreditCard",  appsync_id: String): Account(account_name, account_type, appsync_id) {
    var card_provider = card_provider
    var card_holder = card_holder
    var card_number = card_number
    var cvc = cvc
    var expiration_date = expiration_date
    var notes = notes

}