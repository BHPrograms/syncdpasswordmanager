package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(inheritSuperIndices = true)
class DebitCard(account_name: String, notes: String, card_provider: String, card_holder: String, card_number: String,
                cvc: String, expiration_date: String, pin: String, account_type: String = "DebitCard", appsync_id: String) :
                CreditCard(account_name, notes, card_provider, card_holder, card_number,cvc, expiration_date, account_type, appsync_id) {
    var pin = pin
}