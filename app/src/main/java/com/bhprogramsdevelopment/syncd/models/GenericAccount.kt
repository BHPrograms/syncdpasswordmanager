package com.bhprogramsdevelopment.syncd.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(inheritSuperIndices = true)
open class GenericAccount(account_name: String, account_provider: String, username: String, password: String,
                          notes: String, account_type: String = "GenericAccount",  appsync_id: String):
    Account(account_name, account_type, appsync_id)  {
    var account_provider = account_provider
    var username = username
    var password = password
    var notes = notes
}