package com.bhprogramsdevelopment.syncd.data

import android.R.attr.password
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.amplifyframework.api.ApiException
import com.amplifyframework.api.graphql.model.ModelMutation
import com.amplifyframework.api.graphql.model.ModelQuery
import com.amplifyframework.core.Amplify
import com.amplifyframework.datastore.generated.model.UserAccount
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.models.Account
import com.bhprogramsdevelopment.syncd.utilities.KEY_SYNC_PIN
import com.bhprogramsdevelopment.syncd.utilities.KEY_SYNC_PIN_ENABLED
import com.bhprogramsdevelopment.syncd.utilities.SharedPrefs
import com.bhprogramsdevelopment.syncd.utilities.appPinDefault
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.scottyab.aescrypt.AESCrypt
import kotlinx.coroutines.*
import java.security.GeneralSecurityException


class AppSync {
    private lateinit var context: Context
    lateinit var datasource: AccountDataSource
    lateinit var sharedPrefs: SharedPrefs

    //Implements singleton behavior
    companion object {
        private var INSTANCE: AppSync? = null
        fun getInstance(context: Context): AppSync {
            if (INSTANCE == null) {
                INSTANCE = AppSync()
                INSTANCE!!.context = context
                INSTANCE!!.datasource = AccountDataSource.getInstance(context)
                INSTANCE!!.sharedPrefs = SharedPrefs.getInstance(context)
            }
            return INSTANCE!!
        }
    }

    //Remote account is created, then local is updated with remote ID
    fun createUserAccount(json: String) {
        var accountData = json
        if (sharedPrefs.getBool(KEY_SYNC_PIN_ENABLED) && sharedPrefs.getString(KEY_SYNC_PIN)
                .isNotBlank()
        ) {
            val pin = sharedPrefs.getString(KEY_SYNC_PIN)
            try {
                accountData = AESCrypt.encrypt(pin, json)
            } catch (e: Exception) {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(context, "Error syncing account", Toast.LENGTH_SHORT).show()
                    Log.e("AppSync.createUserAcc", "${e.message}")
                }
            }
        }

        val uid = Firebase.auth.currentUser?.uid
        if (uid != null) {
            val userAccount = UserAccount.builder()
                .uid(uid)
                .account(accountData)
                .build()

            Amplify.API.mutate(
                ModelMutation.create(userAccount),
                { response ->
                    //Now that the account is synced, local holds a ref to the remote version
                    datasource.updateAccountWithRemote(json, response.data.id)
                },
                { error: ApiException? ->
                    run {
                        Log.e("AppSync", "Create failed", error)
                    }
                })
        }
    }

    //Existing remote account is updated, if exists
    fun updateUserAccount(account: Account, json: String) {
        var accountData = json
        if (sharedPrefs.getBool(KEY_SYNC_PIN_ENABLED) && sharedPrefs.getString(KEY_SYNC_PIN)
                .isNotBlank()
        ) {
            val pin = sharedPrefs.getString(KEY_SYNC_PIN)
            try {
                accountData = AESCrypt.encrypt(pin, json)
            } catch (e: Exception) {
                CoroutineScope(Dispatchers.Main).launch {
                    Toast.makeText(context, "Error syncing account", Toast.LENGTH_SHORT).show()
                    Log.e("AppSync.updateUserAcc", "${e.message}")
                }
            }
        }

        val uid = Firebase.auth.currentUser?.uid
        if (uid != null) {
            val userAccount = UserAccount.builder()
                .uid(uid)
                .account(accountData)
                .id(account.appsync_id)
                .build()
            Amplify.API.mutate(
                ModelMutation.update(userAccount),
                { response -> },
                { error: ApiException? ->
                    run {
                        Log.e("AppSync", "Update failed", error)
                    }
                })
        }
    }

    //Returns every account for signed in UID, updates all accounts
    fun getAccounts() {
        val uid = Firebase.auth.currentUser?.uid

        if (uid != null) {
            Amplify.API.query(
                ModelQuery.list(UserAccount::class.java, UserAccount.UID.eq(uid)),
                { response ->
                    val validIDs = ArrayList<String>()
                    if (!response.hasErrors()) {
                        for (account in response.data) {
                            if (sharedPrefs.getBool(KEY_SYNC_PIN_ENABLED) && sharedPrefs.getString(
                                    KEY_SYNC_PIN
                                ).isNotBlank()
                            ) {
                                var accountData = account.account
                                if (!checkDecrypted(accountData)) {
                                    try {
                                        val pin = sharedPrefs.getString(KEY_SYNC_PIN)
                                        accountData = AESCrypt.decrypt(pin, account.account)
                                        if (checkDecrypted(accountData)) {
                                            datasource.updateAccountWithRemote(accountData, account.id)
                                            validIDs.add(account.id)
                                        }

                                    } catch (e: Exception) {
                                        CoroutineScope(Dispatchers.Main).launch {
                                            Toast.makeText(
                                                context,
                                                "Error syncing account",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            Log.e("AppSync.getAccounts", "${e.message}")
                                        }
                                    }
                                } else {
                                    if (checkDecrypted(account.account)) {
                                        validIDs.add(account.id)
                                        datasource.updateAccountWithRemote(account.account, account.id)
                                    }
                                }
                            } else {
                                if (checkDecrypted(account.account)) {
                                    validIDs.add(account.id)
                                    datasource.updateAccountWithRemote(account.account, account.id)
                                }
                            }
                        }

                        datasource.deleteOrphans(validIDs)
                    } else {
                        CoroutineScope(Dispatchers.Main).launch {
                            Toast.makeText(context, "Error getting accounts", Toast.LENGTH_SHORT)
                                .show()
                            Log.e("AppSync.getAccount", "" + response.errors)
                        }
                    }
                },
                { error ->
                    CoroutineScope(Dispatchers.Main).launch {
                        Toast.makeText(context, "Error getting accounts", Toast.LENGTH_SHORT).show()
                        Log.e("AppSync.getAccount", "" + error)
                    }
                })
        }
    }

    //Deletes remote account matching stored id
    fun deleteUserAccount(account: Account) {
        val userAccount = UserAccount.Builder()
            .id(account.appsync_id)
            .build()

        Amplify.API.mutate(ModelMutation.delete(userAccount),
            { response -> },
            { error: ApiException? -> Log.e("AppSync", "Delete failed", error) })
    }

    //Checks if account is a JSON formatted string, if not, returns false for not decrypted
    private fun checkDecrypted(account: String): Boolean {
        try {
            val accountListing = Gson().fromJson(account, Account::class.java)
        } catch (e: Exception) {
            return false;
        }
        return true;
    }

}