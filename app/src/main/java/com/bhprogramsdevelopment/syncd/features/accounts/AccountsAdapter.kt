package com.bhprogramsdevelopment.syncd.features.accounts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.models.Account
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccountsAdapter(private var accounts: List<Account>) :
    RecyclerView.Adapter<AccountsAdapter.AccountViewHolder>() {
    //Local data source
    private lateinit var dataSource: AccountDataSource

    //region ViewHolder declaration

    //Holds a reference to every view in the inflated layout
    class AccountViewHolder(val lytAccountItem: ConstraintLayout) : RecyclerView.ViewHolder(lytAccountItem) {
        var accountIcon: ImageView? = null
        var account_name: TextView? = null

        init {
            accountIcon = lytAccountItem.findViewById(R.id.imgAccountIcon)
            account_name = lytAccountItem.findViewById(R.id.lblAccountName)
        }
    }

    //endregion

    //region ViewHolder event functions

    //Inflates ViewHolder views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        dataSource = AccountDataSource.getInstance(parent.context.applicationContext)

        // Inflate view and create viewholder
        val lytAccountItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_account, parent, false) as ConstraintLayout
        val viewHolder =
            AccountViewHolder(
                lytAccountItem
            )

        //OnClick passes position so account can be retrieved
        lytAccountItem.setOnClickListener(View.OnClickListener {
            val position = viewHolder.adapterPosition;
            if (position != RecyclerView.NO_POSITION) {
                viewAccount(it, position)
            }})

        return viewHolder
    }

    //Sets values from dataset on bind
    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        holder.account_name?.text = accounts[position].account_name

        when(accounts[position].account_type) {
            "CreditCard" -> {
                holder.accountIcon?.setImageResource(R.drawable.ic_card)
            }

            "DebitCard" -> {
                holder.accountIcon?.setImageResource(R.drawable.ic_card)
            }

            "EmailAccount" -> {
                holder.accountIcon?.setImageResource(R.drawable.ic_email)
            }

            "OnlineBankAccount" -> {
                holder.accountIcon?.setImageResource(R.drawable.ic_money)
            }
        }
    }

    //endregion

    //region Data manipulation

    fun setData(accounts: List<Account>) {
        this.accounts = accounts
        notifyDataSetChanged()
    }

    override fun getItemCount() = accounts.size

    //endregion

    //region Navigation

    fun viewAccount(v: View, accountPos: Int) {
        CoroutineScope(Dispatchers.Default).launch {
            var accountListing = accounts[accountPos]
            var account: Account? = null

            //Find and retrieve all account data based on type
            when (accountListing.account_type) {
                "CreditCard" -> {
                    account = dataSource.getCreditCard(accountListing.account_name)
                }

                "DebitCard" -> {
                    account = dataSource.getDebitCard(accountListing.account_name)
                }

                "EmailAccount" -> {
                    account = dataSource.getEmailAccount(accountListing.account_name)
                }

                "GenericAccount" -> {
                    account = dataSource.getGenericAccount(accountListing.account_name)
                }

                "OnlineBankAccount" -> {
                    account = dataSource.getOnlineBankAccount(accountListing.account_name)
                }
            }

            val action =
                AccountsFragmentDirections.viewAccount(
                    Gson().toJson(account)
                )
            v.findNavController().navigate(action)
        }
    }

    //endregion


}
