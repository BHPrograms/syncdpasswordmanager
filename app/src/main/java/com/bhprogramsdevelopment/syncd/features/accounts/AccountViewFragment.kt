package com.bhprogramsdevelopment.syncd.features.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.models.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson


class AccountViewFragment : Fragment() {
    //region Member variables

    //UI variables
    private lateinit var layout: View

    //Account variables
    private var args: String? = ""
    private lateinit var accountListing: Account

    //endregion

    //region State events

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        layout = inflater.inflate(R.layout.fragment_account_view, container, false)

        initUi()

        return layout
    }

    //endregion

    //region UI functions

    private fun navHome() {
        val action =
            AccountViewFragmentDirections.viewAccounts()
        layout.findNavController().navigate(action)
    }

    fun initUi() {
        //Set back button in action bar
        activity?.actionBar?.setDisplayHomeAsUpEnabled(true)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navHome()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        //Gets account to view from args
        args = getArguments()?.let { AccountViewFragmentArgs.fromBundle(
            it
        ).account }
        accountListing = Gson().fromJson(args, Account::class.java)

        //Based on type from accountListing, populate UI
        var account: Account? = null
        when (accountListing.account_type) {
            "CreditCard" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, CreditCard::class.java)
                layout.findViewById<TextView>(R.id.lblAccountName).text = account.account_name
                layout.findViewById<TextView>(R.id.lblProvider).text = account.card_provider
                layout.findViewById<TextView>(R.id.lblCardHolder).text = account.card_holder
                layout.findViewById<TextView>(R.id.lblCardNumber).text = account.card_number
                layout.findViewById<TextView>(R.id.lblExpirationDate).text = account.expiration_date
                layout.findViewById<TextView>(R.id.lblCVC).text = account.cvc
                layout.findViewById<TextView>(R.id.lblNotes).text = account.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytCards).visibility = View.VISIBLE
            }
            
            "DebitCard" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, DebitCard::class.java)
                layout.findViewById<TextView>(R.id.lblAccountName).text = account.account_name
                layout.findViewById<TextView>(R.id.lblProvider).text = account.card_provider
                layout.findViewById<TextView>(R.id.lblCardHolder).text = account.card_holder
                layout.findViewById<TextView>(R.id.lblCardNumber).text = account.card_number
                layout.findViewById<TextView>(R.id.lblExpirationDate).text = account.expiration_date
                layout.findViewById<TextView>(R.id.lblCVC).text = account.cvc
                layout.findViewById<TextView>(R.id.lblPin).text = account.pin
                layout.findViewById<TextView>(R.id.lblNotes).text = account.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytCards).visibility = View.VISIBLE
                layout.findViewById<LinearLayout>(R.id.lytDebitPin).visibility = View.VISIBLE
            }

            "EmailAccount" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, EmailAccount::class.java)
                layout.findViewById<TextView>(R.id.lblAccountName).text = account.account_name
                layout.findViewById<TextView>(R.id.lblProvider).text = account.email_provider
                layout.findViewById<TextView>(R.id.lblEmail).text = account.email
                layout.findViewById<TextView>(R.id.lblEmailPassword).text = account.password
                layout.findViewById<TextView>(R.id.lblNotes).text = account.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytEmail).visibility = View.VISIBLE
            }

            "GenericAccount" -> {
                //Set all fields based on retrieved
                account = Gson().fromJson(args, GenericAccount::class.java)
                layout.findViewById<TextView>(R.id.lblAccountName).text = account.account_name
                layout.findViewById<TextView>(R.id.lblProvider).text = account.account_provider
                layout.findViewById<TextView>(R.id.lblGenericUsername).text = account.username
                layout.findViewById<TextView>(R.id.lblGenericPassword).text = account.password
                layout.findViewById<TextView>(R.id.lblNotes).text = account.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytGeneric).visibility = View.VISIBLE
            }

            "OnlineBankAccount" -> {
                //Set all fields based on retrieved
                account = Gson().fromJson(args, OnlineBankAccount::class.java)
                layout.findViewById<TextView>(R.id.lblAccountName).text = account.account_name
                layout.findViewById<TextView>(R.id.lblProvider).text = account.bank_name
                layout.findViewById<TextView>(R.id.lblOnlineBankAccountNumber).text = account.account_number
                layout.findViewById<TextView>(R.id.lblOnlineBankUser).text = account.username
                layout.findViewById<TextView>(R.id.lblOnlineBankPassword).text = account.password
                layout.findViewById<TextView>(R.id.lblOnlineBankPin).text = account.pin
                layout.findViewById<TextView>(R.id.lblNotes).text = account.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytOnlineBankAccount).visibility = View.VISIBLE
            }
        }

        layout.findViewById<FloatingActionButton>(R.id.fabEditAccount).setOnClickListener(View.OnClickListener {
            val action =
                AccountViewFragmentDirections.editAccount(
                    Gson().toJson(account)
                )
            layout.findNavController().navigate(action)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                navHome()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //endregion

}