package com.bhprogramsdevelopment.syncd.features.settings

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.features.accounts.AccountsFragment
import com.bhprogramsdevelopment.syncd.utilities.*


class SyncPinFragment : Fragment() {
    lateinit var layout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_pin_setting, container, false)

        initUi()

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navSyncSettings()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        return layout
    }

    //region Pin functions

    fun setPin() {
        val pin = layout.findViewById<EditText>(R.id.txtAppPin).text.toString()

        if (pin.length == 4) {
            SharedPrefs.getInstance(requireContext().applicationContext).putString(KEY_SYNC_PIN, pin)
            navSyncSettings()
        } else {
            Toast.makeText(requireContext(), "Four digits required for pin", Toast.LENGTH_SHORT).show()
        }
    }

    //endregion

    //region UI functions

    fun initUi() {
        //Set back button in action bar
        activity?.getActionBar()?.setDisplayHomeAsUpEnabled(true)

        layout.findViewById<Button>(R.id.btnSavePin).setOnClickListener(View.OnClickListener {
            setPin()
        })
    }

    fun navSyncSettings() {
        val action = SyncPinFragmentDirections.viewSyncSettings()
        layout.findNavController().navigate(action)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                navSyncSettings()
            }
        }

        //Only handle item selection once, do not call super
        return true
    }

    //endregion

}