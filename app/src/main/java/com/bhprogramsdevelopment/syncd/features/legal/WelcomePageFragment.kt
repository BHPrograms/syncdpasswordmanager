package com.bhprogramsdevelopment.syncd.features.legal
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R

class WelcomePageFragment : Fragment() {
    private lateinit var layout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater.inflate(R.layout.fragment_welcome_page, container, false)

        layout.findViewById<Button>(R.id.btnContinue).setOnClickListener(View.OnClickListener {
            layout.findNavController().navigate(WelcomePageFragmentDirections.viewPrivacyPolicy())
        })

        return layout
    }
}
