package com.bhprogramsdevelopment.syncd.features.accounts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bhprogramsdevelopment.syncd.BuildConfig
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.data.AppSync
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.models.Account
import com.bhprogramsdevelopment.syncd.utilities.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//Activity for all Account fragments
class AccountsActivity : AppCompatActivity() {

    //region State events

    override fun onCreate(savedInstanceState: Bundle?) {
        theme.applyStyle(SharedPrefs.getInstance(applicationContext).getStyle(), true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Perform sync on activity creation (App start or activity recreation)
        val sharedPrefs = SharedPrefs.getInstance(applicationContext)
        if (sharedPrefs.getBool(KEY_SYNC_ENABLED) && Firebase.auth.currentUser != null) {
            val appSync = AppSync.getInstance(applicationContext)
            val accountDataSource = AccountDataSource.getInstance(applicationContext)
            appSync.getAccounts()
            accountDataSource.syncWithRemote()
        }

    }

    //endregion
}