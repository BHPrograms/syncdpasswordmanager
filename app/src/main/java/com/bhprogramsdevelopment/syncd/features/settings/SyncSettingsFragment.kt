package com.bhprogramsdevelopment.syncd.features.settings

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.features.accounts.AccountsFragment
import com.bhprogramsdevelopment.syncd.utilities.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class SyncSettingsFragment : Fragment() {
    lateinit var layout: View
    private lateinit var auth: FirebaseAuth
    private lateinit var sharedPrefs: SharedPrefs

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_sync_settings, container, false)

        auth = Firebase.auth
        sharedPrefs = SharedPrefs.getInstance(requireContext())

        initUi()


        return layout
    }

    //region UI functions

    fun navAccountSettings() {
        layout.findNavController().navigate(SyncSettingsFragmentDirections.viewAccountSettings())
    }

    fun navSyncPin() {
        layout.findNavController().navigate(SyncSettingsFragmentDirections.viewSyncPin())
    }

    fun navSettings() {
        val action = SyncSettingsFragmentDirections.viewSettings()
        layout.findNavController().navigate(action)
    }

    fun initUi() {
        //Set back button in action bar
        activity?.getActionBar()?.setDisplayHomeAsUpEnabled(true)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navSettings()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        layout.findViewById<TextView>(R.id.lblSetupAccountBtn).setOnClickListener(View.OnClickListener {
            navAccountSettings()
        })

        if (auth.currentUser != null) {
            layout.findViewById<LinearLayout>(R.id.lytSignedOut).visibility = View.GONE
            layout.findViewById<LinearLayout>(R.id.lytSignedIn).visibility = View.VISIBLE
            layout.findViewById<TextView>(R.id.lblAccountUsername).text = auth.currentUser!!.email
        }

        layout.findViewById<TextView>(R.id.lblSignOutBtn).setOnClickListener(View.OnClickListener {
            auth.signOut()
            requireActivity().recreate()
        })

        layout.findViewById<CheckBox>(R.id.chkSync).apply{
            setOnClickListener(View.OnClickListener {
                if ((it as CheckBox).isChecked) {
                    sharedPrefs.putBool(KEY_SYNC_ENABLED, true)
                    Toast.makeText(requireContext(), "Sync enabled", Toast.LENGTH_SHORT).show()
                } else {
                    sharedPrefs.putBool(KEY_SYNC_ENABLED, false)
                    Toast.makeText(requireContext(), "Sync disabled", Toast.LENGTH_SHORT).show()
                }
            })

            if (sharedPrefs.getBool(KEY_SYNC_ENABLED))
                isChecked = true
        }

        layout.findViewById<CheckBox>(R.id.chkSyncPin).apply{
            setOnClickListener(View.OnClickListener {
                if ((it as CheckBox).isChecked) {
                    sharedPrefs.putBool(KEY_SYNC_PIN_ENABLED, true)
                    Toast.makeText(requireContext(), "Sync pin enabled", Toast.LENGTH_SHORT).show()
                } else {
                    sharedPrefs.putBool(KEY_SYNC_PIN_ENABLED, false)
                    Toast.makeText(requireContext(), "Sync pin disabled", Toast.LENGTH_SHORT).show()
                }
            })

            if (sharedPrefs.getBool(KEY_SYNC_PIN_ENABLED))
                isChecked = true
        }


        layout.findViewById<TextView>(R.id.lblSyncPinBtn).setOnClickListener(View.OnClickListener {
            navSyncPin()
        })

        if (sharedPrefs.getBool(KEY_SYNC_PIN_ENABLED)) {
            layout.findViewById<TextView>(R.id.lblRemoveSyncPinBtn).apply{
                setOnClickListener(View.OnClickListener {
                    sharedPrefs.deletePref(KEY_SYNC_PIN)
                    sharedPrefs.deletePref(KEY_SYNC_PIN_ENABLED)
                    Toast.makeText(requireContext(), "Sync pin has been deleted", Toast.LENGTH_SHORT).show()
                    it.visibility = View.GONE
                })
                visibility = View.VISIBLE
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                navSettings()
            }
        }

        //Only handle item selection once, do not call super
        return true
    }

    //endregion

}