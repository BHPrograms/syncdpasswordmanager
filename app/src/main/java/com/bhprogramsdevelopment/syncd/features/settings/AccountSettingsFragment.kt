package com.bhprogramsdevelopment.syncd.features.settings

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.utilities.regexEmail
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase


class AccountSettingsFragment : Fragment() {
    lateinit var layout: View
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_account_settings, container, false)

        auth = Firebase.auth

        initUi()

        return layout
    }

    //region Authentication methods

    fun login(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(requireContext(), "Login successful", Toast.LENGTH_SHORT).show()
                    navSyncSettings()
                } else {
                    try {
                        throw task.exception!!
                    } catch (e: FirebaseAuthInvalidCredentialsException) {
                        Toast.makeText(requireContext(), "Incorrect username or password", Toast.LENGTH_SHORT).show()
                    } catch (e: java.lang.Exception) {
                        Toast.makeText(requireContext(), "An unknown error has occurred", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    fun createAccount(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    if (auth.currentUser == null) {
                        Toast.makeText(requireContext(), "Account created", Toast.LENGTH_SHORT).show()
                        login(email, password)
                    } else {
                        Toast.makeText(requireContext(), "Account created", Toast.LENGTH_SHORT).show()
                        navSyncSettings()
                    }
                } else {
                    try {
                        throw task.exception!!
                    } catch (e: FirebaseAuthWeakPasswordException) {
                        Toast.makeText(requireContext(), "Password must be at least eight characters long", Toast.LENGTH_SHORT).show()
                    } catch (e: FirebaseAuthUserCollisionException) {
                        Toast.makeText(requireContext(), "An account for this email already exists", Toast.LENGTH_SHORT).show()
                    } catch (e: Exception) {
                        Toast.makeText(requireContext(), "An unknown error has occurred", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    //endregion

    //region UI functions

    fun navSyncSettings() {
        val action = AccountSettingsFragmentDirections.viewSyncSettings()
        layout.findNavController().navigate(action)
    }

    fun initUi() {
        //Set back button in action bar
        activity?.getActionBar()?.setDisplayHomeAsUpEnabled(true)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navSyncSettings()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)


        layout.findViewById<TextView>(R.id.lblCreateAccountBtn)
            .setOnClickListener(View.OnClickListener {
                layout.findViewById<LinearLayout>(R.id.lytLogin).visibility = View.GONE
                layout.findViewById<LinearLayout>(R.id.lytCreateAccount).visibility =
                    View.VISIBLE
            })

        layout.findViewById<TextView>(R.id.lblLoginBtn)
            .setOnClickListener(View.OnClickListener {
                layout.findViewById<LinearLayout>(R.id.lytCreateAccount).visibility = View.GONE
                layout.findViewById<LinearLayout>(R.id.lytLogin).visibility = View.VISIBLE
            })

        layout.findViewById<Button>(R.id.btnLogin).setOnClickListener(View.OnClickListener {
            val email =
                layout.findViewById<EditText>(R.id.txtLoginUsername).text.toString()
            val password =
                layout.findViewById<EditText>(R.id.txtLoginPassword).text.toString()
            login(email, password)
        })

        layout.findViewById<Button>(R.id.btnCreateAccount)
            .setOnClickListener(View.OnClickListener {
                val email =
                    layout.findViewById<EditText>(R.id.txtCreateAccountUsername).text.toString()
                val password =
                    layout.findViewById<EditText>(R.id.txtCreateAccountPassword).text.toString()
                val confirmPassword =
                    layout.findViewById<EditText>(R.id.txtConfirmAccountPassword).text.toString()

                if (regexEmail.toRegex().matches(email)) {
                    if (password.length >= 8) {
                        if (password == confirmPassword) {
                            createAccount(email, password)
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Passwords must match",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Password must be at least eight characters long",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please enter a valid email",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navSyncSettings()
            }
        }

        //Only handle item selection once, do not call super
        return true
    }

    //endregion

}