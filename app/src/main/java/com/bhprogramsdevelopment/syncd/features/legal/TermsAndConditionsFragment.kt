package com.bhprogramsdevelopment.syncd.features.legal

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.features.accounts.AccountsActivity
import com.bhprogramsdevelopment.syncd.utilities.SharedPrefs
import com.bhprogramsdevelopment.syncd.utilities.privacyPolicyPrefsKey
import com.bhprogramsdevelopment.syncd.utilities.termsAndConditionsPrefsKey


class TermsAndConditionsFragment : Fragment() {
    private lateinit var layout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater.inflate(R.layout.fragment_terms_and_conditions, container, false)

        initUI()

        return layout
    }

    fun initUI() {
        layout.findViewById<TextView>(R.id.lblContent).text = Html.fromHtml(getString(R.string.terms_and_conditions))

        layout.findViewById<Button>(R.id.btnContinue).setOnClickListener(View.OnClickListener {
            val sharedPrefs = SharedPrefs.getInstance(requireContext().applicationContext)
            if (layout.findViewById<CheckBox>(R.id.chkAgreement).isChecked) {
                sharedPrefs.putBool(termsAndConditionsPrefsKey, true)

                val intent = Intent(requireContext(), AccountsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                sharedPrefs.putBool(privacyPolicyPrefsKey, false)
                Toast.makeText(requireContext(), "Please agree to continue", Toast.LENGTH_SHORT).show()
            }
        })
    }

}
