package com.bhprogramsdevelopment.syncd.features.accounts

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.data.AppSync
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.features.legal.LegalAgreementActivity
import com.bhprogramsdevelopment.syncd.features.settings.SettingsActivity
import com.bhprogramsdevelopment.syncd.models.Account
import com.bhprogramsdevelopment.syncd.utilities.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

class AccountsFragment : Fragment() {

    //region Member variables

    //Layout variables
    private lateinit var layout: View
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: AccountsAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager

    //Data variables
    private lateinit var disposable: Disposable
    private lateinit var dataSource: AccountDataSource
    private lateinit var sharedPrefs: SharedPrefs
    private lateinit var accounts: List<Account>

    //endregion

    //region Static variables

    companion object {
        //Used to keep track throughout life of app, beyond activity
         var appUnlocked = false
    }

    //endregion

    //region State events
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Init data variables
        accounts = ArrayList<Account>()
        dataSource = AccountDataSource.getInstance(requireContext().applicationContext)
        sharedPrefs = SharedPrefs.getInstance(requireContext().applicationContext)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        layout  = inflater.inflate(R.layout.fragment_accounts, container, false)

        initUI()

        return layout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if (!sharedPrefs.getBool(privacyPolicyPrefsKey) || !sharedPrefs.getBool(termsAndConditionsPrefsKey)) {

            //Checks if legal agreements have been accepted, if not shows relevant fragment
            val intent = Intent(requireContext(), LegalAgreementActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        } else if (sharedPrefs.getBool(KEY_APP_PIN_ENABLED) && sharedPrefs.getString(KEY_APP_PIN).isNotBlank()) {

            //Checks if pin needs to be entered to unlock app, if not shows relevant fragment
            val authenticated = arguments?.let { AccountsFragmentArgs.fromBundle(it).authenticated } ?: false

            if (authenticated) {
                //Prevent lock screen for activity resume/recreate
                appUnlocked = true
            } else if (!authenticated && !appUnlocked) {
                val navController = findNavController()
                navController.navigate(R.id.pinLockFragment)
            }

        }
    }

    //endregion

    //region UI functions

    fun initUI() {
        viewManager = LinearLayoutManager(activity?.applicationContext)
        viewAdapter = AccountsAdapter(accounts)

        recyclerView = layout.findViewById<RecyclerView>(R.id.recycAccounts).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        disposable = dataSource.getAllAccounts()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(Consumer<List<Account?>?> {
                viewAdapter.setData(it as List<Account>)
                if (viewAdapter.itemCount == 0) {
                    layout.findViewById<TextView>(R.id.lblNoAccounts).visibility = View.VISIBLE
                } else {
                    layout.findViewById<TextView>(R.id.lblNoAccounts).visibility = View.GONE
                }
            })!!

        layout.findViewById<FloatingActionButton>(R.id.fabCreateAccount).setOnClickListener(View.OnClickListener {
            it.findNavController().navigate(AccountsFragmentDirections.createAccount())
        })

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_settings, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(requireContext(), SettingsActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //endregion
}