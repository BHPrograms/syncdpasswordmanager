package com.bhprogramsdevelopment.syncd.features.settings

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.utilities.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        theme.applyStyle(SharedPrefs.getInstance(applicationContext).getStyle(), true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }
}