package com.bhprogramsdevelopment.syncd.features.accounts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.models.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccountEditFragment : Fragment() {

    //region Member variables

    //Local data source
    private lateinit var dataSource: AccountDataSource

    //UI container
    private lateinit var layout: View

    //Holds args passed in and corresponding account info
    private var args: String? = ""
    private lateinit var account: Account
    private lateinit var account_type: String

    //endregion


    //region State events

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout 
        layout = inflater.inflate(R.layout.fragment_account_edit, container, false)
        initUi()

        //Set data source, for saving/deletion
        dataSource = AccountDataSource.getInstance(requireContext().applicationContext)

        return layout
    }

    //endregion

    //region Account manipulation

    private fun deleteAccount() {
        CoroutineScope(Dispatchers.Default).launch {
            args?.let {
                dataSource.deleteAccount(it)
                navHome()
            }
        }
    }

    private fun updateAccount(deleteOld: Boolean = false) {
        CoroutineScope(Dispatchers.Default).launch {
            if (deleteOld) {
                args?.let { dataSource.deleteAccount(it) }
                dataSource.updateAccount(getAccount())

            } else {
                dataSource.updateAccount(getAccount())
            }

            navHome()
        }
    }

    private fun saveAccount() {
        CoroutineScope(Dispatchers.Default).launch {
            val account_name = layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
            val accountExists = dataSource.checkAccountExists(account_name)

            if (accountExists && account.account_name != account_name) {
                CoroutineScope(Dispatchers.Main).launch {
                    MaterialAlertDialogBuilder(requireContext(), R.style.StandardDialog)
                        .setTitle(resources.getString(R.string.save_account_title))
                        .setMessage(resources.getString(R.string.save_account_message))
                        .setNegativeButton(resources.getString(R.string.save_account_cancel_button)) { dialog, which -> }
                        .setPositiveButton(resources.getString(R.string.save_account_confirm_button)) { dialog, which ->
                            args?.let {
                                updateAccount(true)
                            }
                        }
                        .show()
                }
            } else {
                args?.let {
                    updateAccount()
                }
            }
        }
    }

    private fun getAccount(): String {
        //Based on type from accountListing, populate UI
        when (account_type) {
            "CreditCard" -> {
                val account_name =
                    layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
                val card_provider = layout.findViewById<TextView>(R.id.txtProvider).text.toString()
                var card_holder = layout.findViewById<TextView>(R.id.txtCardHolder).text.toString()
                val card_number = layout.findViewById<TextView>(R.id.txtCardNumber).text.toString()
                val expiration_date =
                    layout.findViewById<TextView>(R.id.txtExpirationDate).text.toString()
                val cvc = layout.findViewById<TextView>(R.id.txtCVC).text.toString()
                val notes = layout.findViewById<TextView>(R.id.txtNotes).text.toString()

                return Gson().toJson(
                    CreditCard(
                        account_name = account_name,
                        notes = notes,
                        card_provider = card_provider,
                        card_holder = card_holder,
                        card_number = card_number,
                        cvc = cvc,
                        expiration_date = expiration_date, appsync_id = account.appsync_id))
            }

            "DebitCard" -> {
                val account_name =
                    layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
                val card_provider = layout.findViewById<TextView>(R.id.txtProvider).text.toString()
                var card_holder = layout.findViewById<TextView>(R.id.txtCardHolder).text.toString()
                val card_number = layout.findViewById<TextView>(R.id.txtCardNumber).text.toString()
                val expiration_date =
                    layout.findViewById<TextView>(R.id.txtExpirationDate).text.toString()
                val cvc = layout.findViewById<TextView>(R.id.txtCVC).text.toString()
                val pin = layout.findViewById<TextView>(R.id.txtPin).text.toString()
                val notes = layout.findViewById<TextView>(R.id.txtNotes).text.toString()

                return Gson().toJson(
                    DebitCard(
                        account_name = account_name,
                        notes = notes,
                        card_provider = card_provider,
                        card_holder = card_holder,
                        card_number = card_number,
                        cvc = cvc,
                        expiration_date = expiration_date,
                        pin = pin
                        , appsync_id = account.appsync_id)
                    )
            }

            "EmailAccount" -> {
                val account_name =
                    layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
                val emailProvider = layout.findViewById<TextView>(R.id.txtProvider).text.toString()
                val email = layout.findViewById<TextView>(R.id.txtEmail).text.toString()
                val password = layout.findViewById<TextView>(R.id.txtEmailPassword).text.toString()
                val notes = layout.findViewById<TextView>(R.id.txtNotes).text.toString()

                return Gson().toJson(
                    EmailAccount(
                        account_name = account_name, notes = notes,
                        email_provider = emailProvider, email = email, password = password, appsync_id = account.appsync_id)
                    )
            }

            "GenericAccount" -> {
                val account_name =
                    layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
                val accountProvider =
                    layout.findViewById<TextView>(R.id.txtProvider).text.toString()
                val username =
                    layout.findViewById<TextView>(R.id.txtGenericUsername).text.toString()
                val password =
                    layout.findViewById<TextView>(R.id.txtGenericPassword).text.toString()
                val notes = layout.findViewById<TextView>(R.id.txtNotes).text.toString()

                return Gson().toJson(
                    GenericAccount(
                        account_name, account_provider = accountProvider,
                        username = username, password = password, notes = notes, appsync_id = account.appsync_id)
                )
            }

            "OnlineBankAccount" -> {
                val account_name =
                    layout.findViewById<TextView>(R.id.txtAccountName).text.toString()
                val bankName = layout.findViewById<TextView>(R.id.txtProvider).text.toString()
                val accountNumber =
                    layout.findViewById<TextView>(R.id.txtOnlineBankAccountNumber).text.toString()
                val username = layout.findViewById<TextView>(R.id.txtOnlineBankUser).text.toString()
                val password =
                    layout.findViewById<TextView>(R.id.txtOnlineBankPassword).text.toString()
                val pin = layout.findViewById<TextView>(R.id.txtOnlineBankPin).text.toString()
                val notes = layout.findViewById<TextView>(R.id.txtNotes).text.toString()

                return Gson().toJson(
                    OnlineBankAccount(
                        account_name = account_name,
                        notes = notes,
                        bank_name = bankName,
                        account_number = accountNumber,
                        username = username,
                        password = password,
                        pin = pin
                        , appsync_id = account.appsync_id)
                    )
            }
        }

        return ""
    }

    //endregion


    //region UI methods

    fun initUi() {
        //Overides onbackpressed
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navView()
        }

        //Creates back arrow
        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        //Set back button in action bar
        activity?.getActionBar()?.setDisplayHomeAsUpEnabled(true)

        //Gets passed in account information for use in views, and later data processes
        args = getArguments()?.let { AccountEditFragmentArgs.fromBundle(it).account }
        val accountListing = Gson().fromJson(args, Account::class.java)
        account_type = accountListing.account_type

        //Fills in fields
        populateUI()

        //Material Design dialog, for confirming deletion
        layout.findViewById<com.github.clans.fab.FloatingActionButton>(R.id.fabDelete)
            .setOnClickListener(
                View.OnClickListener {
                    MaterialAlertDialogBuilder(requireContext(), R.style.StandardDialog)
                        .setTitle(resources.getString(R.string.delete_account_title))
                        .setMessage(resources.getString(R.string.delete_account_message))
                        .setNegativeButton(resources.getString(R.string.delete_account_cancel_button)) { dialog, which -> }
                        .setPositiveButton(resources.getString(R.string.delete_account_confirm_button)) { dialog, which ->
                            CoroutineScope(Dispatchers.Default).launch {
                                deleteAccount()
                            }
                        }
                        .show()
                })

        //Onclick to save changes
        layout.findViewById<com.github.clans.fab.FloatingActionButton>(R.id.fabSave)
            .setOnClickListener(
                View.OnClickListener {
                    CoroutineScope(Dispatchers.Default).launch {
                        saveAccount()
                    }
                })

    }

    //Prefills fields for account
    fun populateUI() {
        //Based on type from accountListing, populate UI
        when (account_type) {
            "CreditCard" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, CreditCard::class.java)
                val creditCard = account as CreditCard

                layout.findViewById<TextView>(R.id.txtAccountName).text = creditCard.account_name
                layout.findViewById<TextView>(R.id.txtProvider).text = creditCard.card_provider
                layout.findViewById<TextView>(R.id.txtCardHolder).text = creditCard.card_holder
                layout.findViewById<TextView>(R.id.txtCardNumber).text = creditCard.card_number
                layout.findViewById<TextView>(R.id.txtExpirationDate).text =
                    creditCard.expiration_date
                layout.findViewById<TextView>(R.id.txtCVC).text = creditCard.cvc
                layout.findViewById<TextView>(R.id.txtNotes).text = creditCard.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytCards).visibility = View.VISIBLE
            }

            "DebitCard" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, DebitCard::class.java)
                val debitCard = account as DebitCard

                layout.findViewById<TextView>(R.id.txtAccountName).text = debitCard.account_name
                layout.findViewById<TextView>(R.id.txtProvider).text = debitCard.card_provider
                layout.findViewById<TextView>(R.id.txtCardHolder).text = debitCard.card_holder
                layout.findViewById<TextView>(R.id.txtCardNumber).text = debitCard.card_number
                layout.findViewById<TextView>(R.id.txtExpirationDate).text =
                    debitCard.expiration_date
                layout.findViewById<TextView>(R.id.txtCVC).text = debitCard.cvc
                layout.findViewById<TextView>(R.id.txtPin).text = debitCard.pin
                layout.findViewById<TextView>(R.id.txtNotes).text = debitCard.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytCards).visibility = View.VISIBLE
                layout.findViewById<LinearLayout>(R.id.lytDebitPin).visibility = View.VISIBLE
            }

            "EmailAccount" -> {
                //Set all fields based on retrieved object
                account = Gson().fromJson(args, EmailAccount::class.java)
                val email = account as EmailAccount

                layout.findViewById<TextView>(R.id.txtAccountName).text = email.account_name
                layout.findViewById<TextView>(R.id.txtProvider).text = email.email_provider
                layout.findViewById<TextView>(R.id.txtEmail).text = email.email
                layout.findViewById<TextView>(R.id.txtEmailPassword).text = email.password
                layout.findViewById<TextView>(R.id.txtNotes).text = email.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytEmail).visibility = View.VISIBLE
            }

            "GenericAccount" -> {
                //Set all fields based on retrieved
                account = Gson().fromJson(args, GenericAccount::class.java)
                val genericAccount = account as GenericAccount

                layout.findViewById<TextView>(R.id.txtAccountName).text =
                    genericAccount.account_name
                layout.findViewById<TextView>(R.id.txtProvider).text =
                    genericAccount.account_provider
                layout.findViewById<TextView>(R.id.txtGenericUsername).text =
                    genericAccount.username
                layout.findViewById<TextView>(R.id.txtGenericPassword).text =
                    genericAccount.password
                layout.findViewById<TextView>(R.id.txtNotes).text = genericAccount.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytGeneric).visibility = View.VISIBLE
            }

            "OnlineBankAccount" -> {
                //Set all fields based on retrieved
                account = Gson().fromJson(args, OnlineBankAccount::class.java)
                val onlineBankAccount = account as OnlineBankAccount

                layout.findViewById<TextView>(R.id.txtAccountName).text =
                    onlineBankAccount.account_name
                layout.findViewById<TextView>(R.id.txtProvider).text =
                    onlineBankAccount.account_provider
                layout.findViewById<TextView>(R.id.txtOnlineBankAccountNumber).text =
                    onlineBankAccount.account_number
                layout.findViewById<TextView>(R.id.txtOnlineBankUser).text =
                    onlineBankAccount.username
                layout.findViewById<TextView>(R.id.txtOnlineBankPassword).text =
                    onlineBankAccount.password
                layout.findViewById<TextView>(R.id.txtOnlineBankPin).text = onlineBankAccount.pin
                layout.findViewById<TextView>(R.id.txtNotes).text = onlineBankAccount.notes

                //Set proper visibility
                layout.findViewById<LinearLayout>(R.id.lytOnlineBankAccount).visibility =
                    View.VISIBLE
            }
        }
    }

    private fun navView() {
        val action =
            AccountEditFragmentDirections.viewAccount(
                Gson().toJson(account)
            )
        layout.findNavController().navigate(action)
    }

    private fun navHome() {
        val action =
            AccountEditFragmentDirections.viewAccounts()
        layout.findNavController().navigate(action)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                navView()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //endregion

}