package com.bhprogramsdevelopment.syncd.features.legal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bhprogramsdevelopment.syncd.R

class LegalAgreementActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_legal_agreement)
    }
}