package com.bhprogramsdevelopment.syncd.features.settings

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.NavUtils
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.utilities.*


class SettingsFragment : Fragment() {
    lateinit var layout: View
    lateinit var sharedPrefs: SharedPrefs

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_settings, container, false)

        sharedPrefs = SharedPrefs.getInstance(requireContext().applicationContext)

        initUi()

        return layout
    }

    //region Preference functions

    fun toggleTheme(switch: SwitchCompat) {
        if (switch.isChecked) {
            SharedPrefs.getInstance(requireContext().applicationContext).putString(KEY_APP_THEME, appThemeDark)
            val intent = Intent(requireContext(), SettingsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        } else {
            SharedPrefs.getInstance(requireContext().applicationContext).putString(KEY_APP_THEME, appThemeDefault)
            val intent = Intent(requireContext(), SettingsActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    //endregion

    //region UI functions

    private fun navHome() {
        NavUtils.navigateUpFromSameTask(requireActivity())
    }

    private fun navAppPin() {
        val action =
            SettingsFragmentDirections.setAppPin()
        layout.findNavController().navigate(action)
    }

    private fun navSyncSettings() {
        layout.findNavController().navigate(SettingsFragmentDirections.viewSyncSettings())
    }

    fun initUi() {
        //Set back button in action bar
        activity?.getActionBar()?.setDisplayHomeAsUpEnabled(true)

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navHome()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        layout.findViewById<SwitchCompat>(R.id.swTheme).apply {
            val theme = sharedPrefs.getString(KEY_APP_THEME)

            if (theme == appThemeDark)
                isChecked = true

            setOnClickListener(View.OnClickListener { toggleTheme(it as SwitchCompat) })
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navHome()
        }

        layout.findViewById<TextView>(R.id.lblSyncSettingsBtn).setOnClickListener(View.OnClickListener {
            navSyncSettings()
        })

        layout.findViewById<TextView>(R.id.lblAppPinBtn).setOnClickListener(View.OnClickListener {
            navAppPin()
        })

        layout.findViewById<CheckBox>(R.id.chkPin).apply{
            setOnClickListener(View.OnClickListener {
                if ((it as CheckBox).isChecked) {
                    sharedPrefs.putBool(KEY_APP_PIN_ENABLED, true)
                    Toast.makeText(requireContext(), "App pin enabled", Toast.LENGTH_SHORT).show()
                } else {
                    sharedPrefs.putBool(KEY_APP_PIN_ENABLED, false)
                    Toast.makeText(requireContext(), "App pin disabled", Toast.LENGTH_SHORT).show()
                }
            })

            if (sharedPrefs.getBool(KEY_APP_PIN_ENABLED))
                isChecked = true
            }

        if (sharedPrefs.getBool(KEY_APP_PIN_ENABLED)) {
            layout.findViewById<TextView>(R.id.lblRemoveAppPinBtn).apply{
                setOnClickListener(View.OnClickListener {
                    sharedPrefs.deletePref(KEY_APP_PIN)
                    sharedPrefs.deletePref(KEY_APP_PIN_ENABLED)
                    Toast.makeText(requireContext(), "Saved pin has been deleted", Toast.LENGTH_SHORT).show()
                    it.visibility = View.GONE
                })
                visibility = View.VISIBLE
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                navHome()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //endregion

}