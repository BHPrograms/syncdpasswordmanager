package com.bhprogramsdevelopment.syncd.features.legal

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.utilities.SharedPrefs
import com.bhprogramsdevelopment.syncd.utilities.privacyPolicyPrefsKey


class PrivacyPolicyFragment : Fragment() {
    private lateinit var layout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        layout = inflater.inflate(R.layout.fragment_privacy_policy, container, false)

        initUI()

        return layout
    }

    fun initUI() {
        layout.findViewById<TextView>(R.id.lblContent).text = Html.fromHtml(getString(R.string.privacy_policy))

        layout.findViewById<Button>(R.id.btnContinue).setOnClickListener(View.OnClickListener {
            val sharedPrefs = SharedPrefs.getInstance(requireContext().applicationContext)
            if (layout.findViewById<CheckBox>(R.id.chkAgreement).isChecked) {
                sharedPrefs.putBool(privacyPolicyPrefsKey, true)
                layout.findNavController().navigate(PrivacyPolicyFragmentDirections.viewTermsAndConditions())
            } else {
                sharedPrefs.putBool(privacyPolicyPrefsKey, false)
                Toast.makeText(requireContext(), "Please agree to continue", Toast.LENGTH_SHORT).show()
            }
        })
    }
}
