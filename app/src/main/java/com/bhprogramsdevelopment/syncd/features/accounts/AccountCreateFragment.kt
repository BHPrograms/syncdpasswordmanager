package com.bhprogramsdevelopment.syncd.features.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.db.AccountDataSource
import com.bhprogramsdevelopment.syncd.utilities.SharedPrefs
import com.bhprogramsdevelopment.syncd.utilities.appThemeDarkStyle
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.angmarch.views.NiceSpinner
import org.angmarch.views.OnSpinnerItemSelectedListener
import java.util.*


class AccountCreateFragment : Fragment(), OnSpinnerItemSelectedListener {
    //region Member variables

    //Reference to UI container
    private lateinit var layout: View

    //Currently selected account type
    private var accountType: String = ""

    //Local datasource for account manipulation
    private lateinit var dataSource: AccountDataSource

    //endregion

    //region State events

    //Called when first started
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataSource = AccountDataSource.getInstance(requireContext().applicationContext)
    }

    //Called when views are created, returns inflated views
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_account_create, container, false)

        initUI()

        return layout
    }

    //endregion
    
    //region Account manipulation

    private fun saveAccount() {
        val accountName = layout.findViewById<EditText>(R.id.txtAccountName).text.toString()
        val accountProvider = layout.findViewById<EditText>(R.id.txtProvider).text.toString()
        val notes = layout.findViewById<EditText>(R.id.txtNotes).text.toString()

        if (accountName.isEmpty() || accountProvider.isEmpty()) {
            Toast.makeText(
                requireContext().applicationContext,
                "All required fields must be filled out",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            when (accountType) {
                "Generic Account" -> {
                    val username =
                        layout.findViewById<EditText>(R.id.txtGenericUsername).text.toString()
                    val password =
                        layout.findViewById<EditText>(R.id.txtGenericPassword).text.toString()

                    if (username.isEmpty() || password.isEmpty()) {
                        Toast.makeText(
                            requireContext().applicationContext,
                            "All required fields must be filled out",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        CoroutineScope(Dispatchers.Default).launch {
                            dataSource.createGenericAccount(
                                accountName = accountName, accountProvider = accountProvider,
                                username = username, password = password, notes = notes
                            )
                            navHome()
                        }
                    }
                }

                "Email Account" -> {
                    val email = layout.findViewById<EditText>(R.id.txtEmail).text.toString()
                    val password =
                        layout.findViewById<EditText>(R.id.txtEmailPassword).text.toString()
                    if (email.isEmpty() || password.isEmpty()) {
                        Toast.makeText(
                            requireContext().applicationContext,
                            "All required fields must be filled out",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        CoroutineScope(Dispatchers.Default).launch {
                            dataSource.createEmailAccount(
                                accountName = accountName, notes = notes,
                                emailProvider = accountProvider, email = email, password = password
                            )
                            navHome()
                        }
                    }
                }

                "Credit Card" -> {
                    val cardHolder =
                        layout.findViewById<EditText>(R.id.txtCardHolder).text.toString()
                    val cardNumber =
                        layout.findViewById<EditText>(R.id.txtCardNumber).text.toString()
                    val expirationDate =
                        layout.findViewById<EditText>(R.id.txtExpirationDate).text.toString()
                    val cvc = layout.findViewById<EditText>(R.id.txtCVC).text.toString()

                    if (cardHolder.isEmpty() || cardNumber.isEmpty() || expirationDate.isEmpty() || cvc.isEmpty()) {
                        Toast.makeText(
                            requireContext().applicationContext,
                            "All required fields must be filled out",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        CoroutineScope(Dispatchers.Default).launch {
                            dataSource.createCreditCardAccount(
                                accountName = accountName,
                                notes = notes,
                                cardProvider = accountProvider,
                                cardHolder = cardHolder,
                                cardNumber = cardNumber,
                                cvc = cvc,
                                expirationDate = expirationDate
                            )
                            navHome()
                        }
                    }
                }

                "Debit Card" -> {
                    val cardHolder =
                        layout.findViewById<EditText>(R.id.txtCardHolder).text.toString()
                    val cardNumber =
                        layout.findViewById<EditText>(R.id.txtCardNumber).text.toString()
                    val expirationDate =
                        layout.findViewById<EditText>(R.id.txtExpirationDate).text.toString()
                    val cvc = layout.findViewById<EditText>(R.id.txtCVC).text.toString()
                    val pin = layout.findViewById<EditText>(R.id.txtPin).text.toString()
                    if (cardHolder.isEmpty() || cardNumber.isEmpty() || expirationDate.isEmpty() || cvc.isEmpty() || pin.isEmpty()) {
                        Toast.makeText(
                            requireContext().applicationContext,
                            "All required fields must be filled out",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        CoroutineScope(Dispatchers.Default).launch {
                            dataSource.createDebitCardAccount(
                                accountName = accountName,
                                notes = notes,
                                cardProvider = accountProvider,
                                cardHolder = cardHolder,
                                cardNumber = cardNumber,
                                cvc = cvc,
                                expirationDate = expirationDate,
                                pin = pin
                            )
                            navHome()
                        }
                    }
                }

                "Online Bank Account" -> {
                    val accountNumber =
                        layout.findViewById<EditText>(R.id.txtOnlineBankAccountNumber).text.toString()
                    val user = layout.findViewById<EditText>(R.id.txtOnlineBankUser).text.toString()
                    val pass =
                        layout.findViewById<EditText>(R.id.txtOnlineBankPassword).text.toString()
                    val pin = layout.findViewById<EditText>(R.id.txtOnlineBankPin).text.toString()

                    if (accountNumber.isEmpty() || user.isEmpty() || pass.isEmpty() || pin.isEmpty()) {
                        Toast.makeText(
                            requireContext().applicationContext,
                            "All required fields must be filled out",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        CoroutineScope(Dispatchers.Default).launch {
                            dataSource.createOnlineBankAccount(
                                accountName, notes, accountProvider,
                                accountNumber, user, pin, pass
                            )
                            navHome()
                        }
                    }
                }

            }
        }
    }

    private fun createAccount() {
        val accountName = layout.findViewById<EditText>(R.id.txtAccountName).text.toString()

        CoroutineScope(Dispatchers.Default).launch {
            val accountExists = dataSource.checkAccountExists(accountName)

            if (accountExists) {
                CoroutineScope(Dispatchers.Main).launch {
                    MaterialAlertDialogBuilder(requireContext(), R.style.StandardDialog)
                        .setTitle(resources.getString(R.string.save_account_title))
                        .setMessage(resources.getString(R.string.save_account_message))
                        .setNegativeButton(resources.getString(R.string.save_account_cancel_button)) { dialog, which -> }
                        .setPositiveButton(resources.getString(R.string.save_account_confirm_button)) { dialog, which ->
                            saveAccount()
                        }
                        .show()
                }
            } else {
               saveAccount()
            }
        }

    }
    
    //endregion

    //region UI functions

    fun initUI() {
        val spinner = layout.findViewById(R.id.spnAccountTypes) as NiceSpinner
        val dataset: List<String> = (listOf("Generic Account", "Email Account", "Credit Card", "Debit Card", "Online Bank Account"))
        spinner.attachDataSource(dataset)
        spinner.setOnSpinnerItemSelectedListener(this)


        layout.findViewById<FloatingActionButton>(R.id.fabSaveNewAccount).setOnClickListener(View.OnClickListener {
            createAccount()
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            navHome()
        }

        (requireActivity() as AppCompatActivity).run {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        setHasOptionsMenu(true)

        displayAccountTemplate("Generic Account")
    }

    private fun navHome() {
        layout.findNavController().navigate(AccountCreateFragmentDirections.viewAccounts())
    }

    fun displayAccountTemplate(accountType: String) {
        val lytCards = layout.findViewById<LinearLayout>(R.id.lytCards).apply {visibility = View.GONE}
        val lytDebitPin = layout.findViewById<LinearLayout>(R.id.lytDebitPin).apply {visibility = View.GONE}
        val lytEmail = layout.findViewById<LinearLayout>(R.id.lytEmail).apply {visibility = View.GONE}
        val lytGeneric = layout.findViewById<LinearLayout>(R.id.lytGeneric).apply {visibility = View.GONE}
        val lytOnlineBankAccount = layout.findViewById<LinearLayout>(R.id.lytOnlineBankAccount).apply {visibility = View.GONE}

        when (accountType) {
            "Generic Account" -> {
                lytGeneric.visibility = View.VISIBLE
                this.accountType = "Generic Account"
            }

            "Email Account" -> {
                lytEmail.visibility = View.VISIBLE
                this.accountType = "Email Account"
            }

            "Credit Card" -> {
                lytCards.visibility = View.VISIBLE
                this.accountType = "Credit Card"
            }

            "Debit Card" -> {
                lytCards.visibility = View.VISIBLE
                lytDebitPin.visibility = View.VISIBLE
                this.accountType = "Debit Card"
            }

            "Online Bank Account" -> {
                lytOnlineBankAccount.visibility = View.VISIBLE
                this.accountType = "Online Bank Account"
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                navHome()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemSelected(parent: NiceSpinner?, view: View?, position: Int, id: Long) {
        val accountType = parent?.getItemAtPosition(position).toString()
        displayAccountTemplate(accountType)
    }

    //endregion
}

