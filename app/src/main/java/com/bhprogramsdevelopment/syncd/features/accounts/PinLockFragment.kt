package com.bhprogramsdevelopment.syncd.features.accounts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.bhprogramsdevelopment.syncd.R
import com.bhprogramsdevelopment.syncd.utilities.*


class PinLockFragment : Fragment() {
    //UI variables
    lateinit var layout: View

    //region State events

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_pin_lock, container, false)

        initUi()

        return layout
    }

    //endregion

    //region UI functions

    fun initUi() {
        layout.findViewById<Button>(R.id.btnEvalPin).setOnClickListener(View.OnClickListener {
            evalPin()
        })
    }

    fun navHome() {
        val action =
            PinLockFragmentDirections.viewAccounts(
                true
            )
        layout.findNavController().navigate(action)
    }

    fun evalPin() {
        val inputPin = layout.findViewById<EditText>(R.id.txtAppPin).text.toString()
        val pin = SharedPrefs.getInstance(requireContext().applicationContext).getString(KEY_APP_PIN)

        if (inputPin == pin) {
            navHome()
        } else {
            Toast.makeText(requireContext(), "Invalid pin", Toast.LENGTH_SHORT).show()
        }
    }

    //endregion

}